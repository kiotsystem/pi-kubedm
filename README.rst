**************************
Running Kubernetes in a Pi
**************************

Requirements
############

We recommend using HypriotOS instead of the native raspbian due to the following points:

* On initial boot, HypriotOS automatically resizes the File System.
* Comes preinstalled with Docker 18.04 and Docker Compose
* Comes with cgroups preconfigured
  * e.g. kubernetes requires the cgroups enabled: `memory` and `cpuset`
* Has no swap by default

Reference: https://github.com/hypriot/image-builder-rpi/releases


Set up SSH Keys
###############

Security first, in order to use the ansible playbook, SSH keys are required.

.. code-block:: console

  # we need to generate the keys
  ssh-key-gen
  # as now we have the prv and pub key namely in /tmp/xx and /tmp/xx.pub
  ssh-copy-id -i /tmp/xx.pub user@hostname
  # now we can add it to the shell session with
  ssh-agent
  # add the keys to the agent, the arg -K stores the passphrase in your keychain for you when you add an ssh key to the ssh-agent.
  ssh-add -K /tmp/xx

With this SSH Agent running in the background we can directly interact with the node without the password prompt.

.. code-block:: console

  ssh pirate@black-pearl NODE_IP=192.168.12.2 'bash -s' < init-master.sh


Weave limitations
#################

As HypriotOS and the current Raspbian distribution are released with kernel version 4.14.3X, there's a known bug while using weave net. For reference: https://github.com/weaveworks/weave/issues/3314

RPi Linux kernel version above 4.14.50 should already be able to handle `fastdp`.

To avoid these use the scripts to update the RPi kernel or set the `NO_FASTDP=1` environment variable when executing the `init-master.sh network` script.

Deactivation fastdp is done directly into the weavenet ds/pod definition,

.. code-block:: console

  kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')&env.WEAVE_NO_FASTDP=1"


Make the installation
#####################

The script `init-pi.sh` sets the basic dependencies in a RPi, this has to be run once in all RPi.

.. code-block:: console

  # update the RPi kernel (reboots after its done)
  sudo ./init-pi.sh kernel
  sudo ./init-pi.sh base


The script `init-master.sh` sets the master kubernetes node.

.. code-block:: console

  # log in as sudo (root)
  $ sudo su -
  $ NODE_IP=192.168.123.3 ./init-master.sh kube
  # init the network:
  $ ./init-master.sh network

 
The script `deploy.sh` deploys cross-compatible dependencies. 


Tutorial
########

Simple tutorial in a RPi upgrading the linux kernel

.. code-block:: console

  ssh-copy-id -i /path/to/key.pub pirate@black-pearl
  # set password
  scp -r /path/to/pi-kubeadm pirate@black-pearl:/home/pirate/pi-kubeadm
  ssh -i /path/to/key pirate@black-pearl
  cd pi-kubeadm
  sudo ./init-pi.sh kernel
  # wait for reboot and reconnect through ssh
  cd pi-kubeadm
  sudo ./init-pi.sh base
  ifconfig  # get the node IP
  sudo NODE_IP=###.###.###.### ./init-master.sh kube  # save token and mind the last 20 lines
  # at this point `sudo kubectl get nodes` should be available
  sudo NODE_IP=###.###.###.### ./init-master.sh network
  sudo ./deploy admin-user
  # set now cross kubeproxy
  sudo ./deploy.sh kube-proxy
  sudo ./deploy.sh scope
