#!/bin/bash

###
# Ensure the docker installation
###
function ensure_docker {

  local docker_info=`docker info > /dev/null 2>&1`
  if [ $? -eq 0 ]; then
    echo '--- Docker installed ---'
    return 0
  fi

  # download script
  curl -o /tmp/get-docker.sh https://get.docker.com
  # run script
  sh /tmp/get-docker.sh
  # clean up
  rm -rf /tmp/get-docker.sh
}

###
# Ensure base installation
###
function base_installation {
  local deps=''

  if [ ! -f /sbin/ipvsadm ]; then
    deps="${deps} ipvsadm"
    echo '--- ipvsadm not installed ---'
  fi

  local make_info=`which make`
  if [ -z "$make_info" ]; then
    deps="${deps} make"
    echo '--- make not installed ---'
  fi

  if [ -z "$deps" ]; then
    echo '--- All base deps are installed ---'
    return 0
  fi

  apt update
  apt install -y ${deps}
  rm -rf /var/lib/apt/lists/*
  modprobe ip_vs
  modprobe ip_vs_sh
  modprobe ip_vs_rr
  modprobe ip_vs_wrr
}

###
# Ensure the kubernetes dependencies
###
function ensure_kube {
  local deps=''

  local kubeadm_info=`which kubeadm`
  if [ -z "$kubeadm_info" ]; then
    echo 'kubeadm not installed'
    deps='kubeadm'
  fi

  local kubectl_info=`which kubectl`
  if [ -z "$kubelet_info" ]; then
    echo 'kubectl not installed'
    deps="${deps} kubectl"
  fi

  local kubelet_info=`which kubelet`
  if [ -z "$kubelet_info" ]; then
    echo 'kubelet not installed'
    deps="${deps} kubelet"
  fi

  
  if [ -z "${deps}" ]; then
    echo 'All kube deps are installed'
    return 0
  fi

  apt-get update && apt-get install -y apt-transport-https curl
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
  if [ ! -f /etc/apt/sources.list.d/kubernetes.list ]; then
    echo 'deb http://apt.kubernetes.io/ kubernetes-xenial main' > /etc/apt/sources.list.d/kubernetes.list
  fi
  apt-get update
  apt-get install -y ${deps}
  rm -rf /tmp/get-docker.sh

  echo '---- Info ----'
  kubeadm version

  echo '--- Pulling kube images (waiting: 3-5 minutes) ---'
  kubeadm config images pull
}

###
# Upgrade the kernel version through rpi-update
# https://github.com/Hexxeh/rpi-update
###
function upgrade_kernel {

  if [ ! -f /usr/bin/rpi-update ]; then
    echo '--- rpi-update is not installed ---'
    apt update
    apt install -y rpi-update
    rm -rf /tmp/get-docker.sh
    echo '--- rpi-update installed ---'
  fi

  echo '--- Device will reboot after operation ---'
  rpi-update

  echo '--- rebooting... ---'
  reboot
}


## ----- Commands ----- ##

USER=`id -u`

if [ $USER -ne 0 ]; then
  echo 'Script must be ran with sudo (root) permissions'
  exit 1
fi

if [ "${1}" = 'base' ]; then
  base_installation
  ensure_docker
  ensure_kube
elif [ "${1}" = 'kernel' ]; then
  upgrade_kernel
elif [ "$1" = 'help' ]; then
  echo 'Usage: $ ./init-pi.sh <command>'
  echo '    base    Install the base dependencies'
  echo '    kernel  Upgrade the RPI Kernel (only for rpi)'
  echo '    help    Show help'
else
  echo 'commands: [base|kernel]'
fi
