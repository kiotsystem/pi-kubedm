#!/bin/bash


# https://github.com/kubernetes/dashboard/wiki/Creating-sample-user
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
