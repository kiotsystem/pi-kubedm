#!/bin/bash

###
# Sets scripts defaults, easy set up
###
validate_env() {

  if [ -z "${NODE_IP}" ]; then
    echo '--- NODE_IP is not set'
    return 1
  fi

  # check for the PODS IP CIDR
  if [ -z "${PODS_IP_CIDR}" ]; then
    export PODS_IP_CIDR='10.244.0.0/16'
    echo "--- PODS_IP_CIDR set to ${PODS_IP_CIDR}"
  fi

  if [ -z "${HOME}" ]; then
    echo '--- HOME env variable is not set'
    return 1
  fi

}

###
# Initializes the kubeadm, will make the initial token live for ever, token-ttl=0
#
# :param 1: IP of the kube-api-server (IP of the node)
# :param 2: Pod network Cidr, e.g. '10.244.0.0/16'
# :param 3: Home Path
###
init_kubeadm() {
  local node_ip=${1:?'Please provide the IP of the Kubernetes API server'}
  local pod_network_cidr=${2:?'Please give an addres for the pods cidr'}
  local home=${3:?'Please give the home path'}

  echo '--- Will initialize kubeadm ---'
  kubeadm init --apiserver-advertise-address ${node_ip} \
               --pod-network-cidr ${pod_network_cidr} \
               --token-ttl=0

  echo '--- Finished kubeadm init ---'
  echo ''
  echo '---- Will copy kubeadm configuration ----'
  # copy the configuration to user's home dir
  mkdir -vp ${home}/.kube
  cp -vf /etc/kubernetes/admin.conf ${home}/.kube/config
  chown -v $(id -u):$(id -g) ${home}/.kube/config
  echo '---- Kubeadm configuration in place ----'
}

###
# Prints out a fresh token for nodes to join the cluster
#
# :param 1: token
# :param 2: Token's TTL
###
get_token() {
  local token=${1:?'Please provide a token.'}
  # set the ttl to 0 as default
  local ttl=${2}
  if [ -z "${2}" ]; then
    ttl=0
  fi

  kubeadm token create ${token} --print-join-command --ttl=${ttl}
}

###
# Sets the weave network as plugin
#
# :param 2: pod network cidr
###
set_weave_network() {
  local pod_network_cidr=${1:?'No network Cidr was given for the pods to use.'}
  local kubectl_version=`kubectl version | base64 | tr -d '\n'`


  # deactivate fastdp, refer to README
  opts="env.IPALLOC_RANGE=${pod_network_cidr}"

  if [ ! -z "${NO_FASTDP}" ]; then
    opts="${opts}&env.WEAVE_NO_FASTDP=1"
    echo '--- warning: NO_FASTDP set setting weavenet without fastdp'
  fi

  local params="k8s-version=${kubectl_version}&${opts}"
  local weave_endpoint="https://cloud.weave.works/k8s/v1.10/net.yaml?${params}"

  echo "URL constructed: ${weave_endpoint}"
  kubectl apply -f ${weave_endpoint}
}


# simple print of the help
print_help() {
  echo 'Usage: $ ./init-master.sh <command>'
  echo '    kube        Setup Kubernetes master'
  echo '    network     Setup the network'
  echo '    help        Show help'
  echo ''
  echo '(ENV) VARs:'
  echo '    NODE_IP         The master IP to attach the Kube API server.'
  echo '    HOME            The user home absolute path (given by the shell env)'
  echo '    PODS_IP_CIDR    The Pods IP-CIDR (optional)'
  echo ''
  echo 'Examples: (requires sudo user)'
  echo '# NODE_IP=192.168.153.23 ./init-master.sh kube'
  echo '# PODS_IP_CIDR=10.244.0.0/16 NODE_IP=192.168.153.23 ./init-master.sh kube'
  echo '# ./init-master.sh network  # uses the default pod ip cidr'
  echo ''
}

## ---- Commands ---- #

if [ "$1" = 'help' ]; then
  print_help
  exit 1
fi

USER=`id -u`

if [ $USER -ne 0 ]; then
  echo '--- Script must be ran with sudo (root) permissions'
  print_help
  exit 1
fi

set +e

validate_env
if [ $? -ne 0 ]; then
  echo '--- Environment is not valid'
  exit 1
fi

set -e

if [ "${1}" = 'kube' ]; then
  init_kubeadm ${NODE_IP} ${PODS_IP_CIDR} ${HOME}
elif [ "${1}" = 'network' ]; then
  set_weave_network ${PODS_IP_CIDR}
else
  echo 'commands: [kube|network|help]'
fi
