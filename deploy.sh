#!/bin/bash

set -e


echo '--- Deploying cross-platform services:'

case "$1" in
  admin-user)
    echo '---- Deploying admin-user'
    kubectl apply -R -f ./deploy/admin-user/
    ;;
 
  kube-proxy)
    kubectl apply -R -f ./deploy/kube-proxy/
    echo '---- Listing Daemon Sets'
    kubectl get ds -n kube-system
    echo '---- Deleting old kube-proxy []'
    kubectl delete ds kube-proxy -n kube-system
    echo '---- Listing Daemon Sets'
    kubectl get ds -n kube-system
    ;;

  scope)
    echo '---- Deploying scope'
    kubectl apply -R -f ./deploy/scope/
    ;;
 
  *)
    echo $"Usage: $0 {admin-user|kube-proxy|scope}"
    exit 1
 
esac


echo '--- Finished deploying'
